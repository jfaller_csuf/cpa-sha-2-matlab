%% Clear workspace and vars
clear;
close all;
clc;

%% Params
Fs = 29.5e6;
wnd = 512;
xlsx_fn = 'AnimalSentences';
% xlsx_fn = 'SameLengths';
test_keyword_ind = 1;

%% Load data
[~, keywords] = xlsread(cat(2,'./Data/Keywords_', xlsx_fn ,'.xlsx'));
disp(keywords(test_keyword_ind));
keywords(test_keyword_ind) = [];
trace = readtable(cat(2,'./Data/',xlsx_fn ,'.xlsx'));
num_traces = size(trace,2)-1;

for i=2:num_traces+1
    pow_sig(:,i-1) = table2array(trace(:,i));
end

%% Correlate signals
ind = 1;
for i=1:num_traces
    if(i ~= test_keyword_ind)
        s1 = pow_sig(:,test_keyword_ind);
        s2 = pow_sig(:,i);
        
        % Corr Coeff
        R = corrcoef(s1,s2);
        corr(1,ind) = R(2);
        
        % Delay
        delay = finddelay(s1,s2);
        
        % Align
        if(delay > 0)
            s2 = alignsignals(s2,s1);
        else
            s1 = alignsignals(s1,s2);
        end
        
        % Periodogram
        [s1_pow,f1] = periodogram(s1,[],[],Fs,'power');
        [s2_pow,f2] = periodogram(s2,[],[],Fs,'power');
        
        % Periodogram Diff
        per_diff(1,ind) = max(abs(s1_pow - s2_pow));
        
        % % Corr Periodogram
        % R = corrcoef(s1_pow,s2_pow);
        % per_diff(1,ind) = R(2);
        
        % Magnitude-Squared Coherence
        [Cxy,f] = mscohere(s1,s2,wnd,wnd/2,wnd,Fs);
        % [pks,locs] = findpeaks(Cxy,'MinPeakHeight',0.75);
        
        % Max Magnitude-Squared Coherence
        msc_max_arry(1,ind) = max(Cxy);
        
        % Mean Magnitude-Squared Coherence
        msc_mean_arry(1,ind) = mean(Cxy);
        
        ind = ind + 1;
    end
end

%% Max Corr Output
[conf, ind] = max(corr);
disp(cat(2,num2str(conf),' Max Corr., "', keywords{ind}, '"'));

%% Min Periodogram Diff Output
[per_diff_min, ind] = min(per_diff);
disp(cat(2,num2str(per_diff_min),' Min Periodogram Diff, "', keywords{ind}, '"'));

%% Max Magnitude-Squared Coherence Output
[msc_max, ind] = max(msc_max_arry);
disp(cat(2,num2str(msc_max),' Max Magnitude-Squared Coherence, "', keywords{ind}, '"'));

%% Mean Magnitude-Squared Coherence Output
[msc_mean, ind] = max(msc_mean_arry);
disp(cat(2,num2str(msc_mean),' Max Magnitude-Squared Coherence, "', keywords{ind}, '"'));

%% Corr Stem plot
figure();
stem(corr);
xticks(1:length(keywords));
set(gca,'xticklabels',keywords);
xtickangle(45);
xlabel('Keywords');
ylabel('Corr Coeff');

%% Periodogram Diff Stem plot
figure();
stem(per_diff);
xticks(1:length(keywords));
set(gca,'xticklabels',keywords);
xtickangle(45);
xlabel('Keywords');
ylabel('Periodogram Diff');

%% Max Magnitude-Squared Coherence Stem plot
figure();
stem(msc_max_arry);
xticks(1:length(keywords));
set(gca,'xticklabels',keywords);
xtickangle(45);
xlabel('Keywords');
ylabel('Max Magnitude-Squared Coherence');

%% Mean Magnitude-Squared Coherence Stem plot
figure();
stem(msc_mean_arry);
xticks(1:length(keywords));
set(gca,'xticklabels',keywords);
xtickangle(45);
xlabel('Keywords');
ylabel('Mean Magnitude-Squared Coherence');

