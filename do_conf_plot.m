function do_conf_plot(ind,keywords,fig_title)
    C = confusionmat(keywords,keywords(ind));
    figure();
    cm = confusionchart(C,unique(keywords));
    cm.Title = fig_title;
    cm.NormalizedValues;
    cm.RowSummary = 'row-normalized';
    % cm.ColumnSummary = 'column-normalized';
    set(gca,'Fontsize',20);
    