%% Clear workspace and vars
clear;
close all;
clc;

%% Params
Fs = 29.5e6;
wnd = 512;
xlsx_fn = 'AnimalSentences';
% xlsx_fn = 'SameLengths';

%% Load data
[~, keywords] = xlsread(cat(2,'./Data/Keywords_', xlsx_fn ,'.xlsx'));
trace = readtable(cat(2,'./Data/',xlsx_fn ,'.xlsx'));
num_traces = size(trace,2)-1;

for i=2:num_traces+1
    pow_sig(:,i-1) = table2array(trace(:,i));
end

%% Correlate signals
keywords_ind = 1;
for test_keyword_ind=1:length(keywords)
    for i=1:num_traces
        if(i ~= test_keyword_ind)
            test_keywords(keywords_ind) = keywords(test_keyword_ind);

            s1 = pow_sig(:,test_keyword_ind);
            s2 = pow_sig(:,i);

            % Corr Coeff
            R = corrcoef(s1,s2);
            corr(test_keyword_ind,i) = R(2);

            % Delay
            delay = finddelay(s1,s2);

            % Align
            if(delay > 0)
                s2 = alignsignals(s2,s1);
            else
                s1 = alignsignals(s1,s2);
            end

            % Periodogram
            [s1_pow,f1] = periodogram(s1,[],[],Fs,'power');
            [s2_pow,f2] = periodogram(s2,[],[],Fs,'power');

            % Periodogram Diff
            per_diff(test_keyword_ind,i) = max(abs(s1_pow - s2_pow));

            % % Corr Periodogram
            % R = corrcoef(s1_pow,s2_pow);
            % per_diff(1,ind) = R(2);

            % Magnitude-Squared Coherence
            [Cxy,f] = mscohere(s1,s2,wnd,wnd/2,wnd,Fs);
            % [pks,locs] = findpeaks(Cxy,'MinPeakHeight',0.75);

            % Max Magnitude-Squared Coherence
            msc_max_arry(test_keyword_ind,i) = max(Cxy);

            % Mean Magnitude-Squared Coherence
            msc_mean_arry(test_keyword_ind,i) = mean(Cxy);

            keywords_ind = keywords_ind + 1;
        end
    end
end

%% Max Corr Output
[corr_out, corr_ind] = max(corr);
do_conf_plot(corr_ind,keywords,'Max Corr');

%% Min Periodogram Diff Output
[per_diff_min, per_diff_min_ind] = min(per_diff);
do_conf_plot(per_diff_min_ind,keywords,'Min Periodogram Diff');

%% Max Magnitude-Squared Coherence Output
[msc_max, msc_max_ind] = max(msc_max_arry);
do_conf_plot(msc_max_ind,keywords,'Max Magnitude-Squared Coherence');

%% Mean Magnitude-Squared Coherence Output
[msc_mean, msc_mean_ind] = max(msc_mean_arry);
do_conf_plot(msc_mean_ind,keywords,'Mean Magnitude-Squared Coherence');
